﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DigitalerLaufzettel.Controller;
namespace DigitalerLaufzettel
{
    /// <summary>
    /// Klasse beinhaltet Methoden, die das Speichern von Daten in die Datenbank ermöglichen
    /// </summary>
    public class Speichern
    {

 

        /// <summary>
        /// Speichern der Anmeldedaten
        /// </summary>
        /// <param name="name"></param>
        /// <param name="nachname"></param>
        /// <param name="passwort">Wird aktuell noch nicht verwendet</param>
        /// 
        internal void SpeichernDerAnmeldung(string name, string nachname, string passwort, string email)
        {
            // Hilfsklasse zum Erstellen eines Connectionstrings
            System.Data.SqlClient.SqlConnectionStringBuilder builder =
            new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Data Source"] = @"Bärbel-PC\DIGILAUF"/*@"D3\SQLEXPRESS"*/;
            builder["integrated Security"] = false;
            builder["Initial Catalog"] = "LaufzetData";
            builder["User"] = "sa";
            builder["Password"] = "paul"/*"florim"*/;
            //Console.WriteLine(builder.ConnectionString);

            string meinTollerConnectionString = builder.ConnectionString;


            // native Verbindung zur Datenbank
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = meinTollerConnectionString;

            //Öffnen einer direkten Verbindung zur Datenbank
            conn.Open();

            // variables Script, das an das DBS weitergeleitet wird, wird vorbereitet
            string insertString = string.Format(
            @"INSERT INTO [dbo].[schueler]
            (
                    [schuelername]
                    ,[schuelernachname]
                    ,[passwort]
                    ,[schueler_emailadresse]    )
            
            VALUES  (
                    '{0}', 
                    '{1}',
                    '{2}',
                    '{3}'
                    ) ", name, nachname, passwort, email);

            // Verbindung zwischen Connection und Script
            SqlCommand command = new SqlCommand(insertString, conn);

            // Command wird ausgeführt
            command.ExecuteNonQuery();

            // Connection wird geschlossen
            conn.Close();
        }


        internal void SpeichernKurs(string name, Guid Lehrer_UID, Guid ersterTermin, Guid zweiterTermin)
        {

            Guid kurs_UID = Guid.NewGuid();
            SqlConnection con = createConnection();
            con.Open();
            string insertString = string.Format(
                @"INSERT INTO [dbo].[kurs]
            (
                    [kurs_UID],
                    [name]
                       )
            
            VALUES  (
                    '{0}',
                    '{1}'
                    )", kurs_UID, name);

            SqlCommand cmd = new SqlCommand(insertString, con);
            cmd.ExecuteNonQuery();

            string insertStringLehrerKurs = string.Format(
                @"INSERT INTO [dbo].[lehrer_kurs]
            (
                    [lehrer_UID],
                    [kurs_UID]
                       )
            
            VALUES  (
                    '{0}',
                    '{1}'
                    )", Lehrer_UID, kurs_UID);
            SqlCommand cmdLehrerKurs = new SqlCommand(insertStringLehrerKurs, con);
            cmdLehrerKurs.ExecuteNonQuery();



            Guid[] tempArray = new Guid[2];
            tempArray[0] = ersterTermin;
            tempArray[1] = zweiterTermin;

            
            string InsertStringKursTermin = string.Format(@" INSERT INTO kurs_termin
                            (
                            [kurs_UID],
                            [termin_UID]

                            )Values(
                            '{0}',
                            '{1}'

                            )", kurs_UID, ersterTermin);

            SqlCommand command = new SqlCommand(InsertStringKursTermin, con);
            command.ExecuteNonQuery();


            string ZweiterStringKursTermin = string.Format(@"INSERT INTO kurs_termin
                            (
                            [kurs_UID],
                            [termin_UID]

                            )Values(
                            '{0}',
                            '{1}'

                            )", kurs_UID, zweiterTermin);
            SqlCommand command2 = new SqlCommand(ZweiterStringKursTermin, con);
            command2.ExecuteNonQuery();
            
            con.Close();

        }

        internal void SpeichernKursFuerSchueler(Guid schueler_UID, Guid kurs_UID)
        {
            SqlConnection con = createConnection();
            con.Open();
            string insertString = string.Format(
                @"INSERT INTO [dbo].[schueler_kurs]
            (
                    [schueler_UID], 
                    [kurs_UID]
                       )
            
            VALUES  (
                    '{0}',
                    '{1}'
                    )", schueler_UID, kurs_UID);

            SqlCommand cmd = new SqlCommand(insertString, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        internal void SchuelerBestätigtExkursion(string Tag, int ErsteStunde, int LetzteStunde, Guid schueler_UID, Guid exkursion_UID) {
            SqlConnection conn = createConnection();
            conn.Open();
            string QueryString = string.Format(@"select lehrer.lehrer_UID, kurs.kurs_UID
                            FROM lehrer 
                            Inner Join lehrer_kurs on lehrer.lehrer_UID = lehrer_kurs.lehrer_UID 
                            Inner Join kurs on lehrer_kurs.kurs_UID = kurs.kurs_UID
                            Inner join kurs_termin on kurs.kurs_UID = kurs_termin.kurs_UID 
                            Inner Join kurstermin on kurs_termin.termin_UID = kurstermin.termin_UID
                            Where kurstermin.Tag LIKE '{0}' AND kurstermin.Stundenband > {1} And kurstermin.Stundenband < {2} AND lehrer.lehrer_UID IN(
                            Select lehrer.lehrer_UID
                            From lehrer
                            Inner Join lehrer_kurs on lehrer.lehrer_UID = lehrer_kurs.lehrer_UID 
                            Inner Join kurs on lehrer_kurs.kurs_UID = kurs.kurs_UID
                            Inner join schueler_kurs on kurs.kurs_UID = schueler_kurs.kurs_UID 
                            Inner join schueler on schueler_kurs.schueler_UID = schueler.schueler_UID
                            Where schueler.schueler_UID LIKE '{3}'
                            )
                            Group By lehrer.lehrer_UID, kurs.kurs_UID", Tag, ErsteStunde, LetzteStunde, schueler_UID);

            SqlCommand command = new SqlCommand(QueryString, conn);
            List<Guid[]> tempGuidListe = new List<Guid[]>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Guid[] tempGuid = new Guid[2];
                    tempGuid[0] = reader.GetGuid(0);
                    tempGuid[1] = reader.GetGuid(1);
                    tempGuidListe.Add(tempGuid);
                }
                reader.Close();
            }


            foreach (Guid[] eineGuid in tempGuidListe) {

                    string Querystring = string.Format(@"Insert into lehrer_erlaubt_exkursion(
                        lehrer_UID,
                        kurs_UID,
                        schueler_UID,
                        erlaubt,
                        gelesen,
                        exkursion_UID

                    )Values(
                        '{0}',
                        '{1}',
                        '{2}',
                       '{3}',
                        '{4}',
                        '{5}'
                    )
               ", eineGuid[0], eineGuid[1], schueler_UID, "False", "False", exkursion_UID);

                using (SqlCommand commandLehrerErlaubtExkursion = new SqlCommand(Querystring, conn))
                {
                    commandLehrerErlaubtExkursion.ExecuteNonQuery();
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////string UpDateString = string.Format("Update");





            conn.Close();
        }


        internal void SpeichernExkursion(Guid kurs_UID, string tag, int beginn_stundenband, int ende_stundenband, DateTime datum, Guid LehrerUID, string ort) {
            // 1. Kurs-UID erzeugen
            Guid exkursion_UID = Guid.NewGuid();

            // 2. Datensatz in Exkursionstabelle speichern
            SqlConnection con = createConnection();
            con.Open();
            string insertString = string.Format(
                @"INSERT INTO [dbo].[exkursion]
            (
                    [kurs_UID],
                    [tag],
                    [beginn_stundenband],
                    [ende_stundenband],
                    [datum],
                    [exkursion_UID],
                    [ort]
                       )
         
            VALUES  (
                    '{0}',
                    '{1}',
                    '{2}',      
                    '{3}',
                    '{4}',
                    '{5}',
                    '{6}'
                    
                    )", kurs_UID,tag,beginn_stundenband,ende_stundenband,datum.ToShortDateString().Substring(0,10),exkursion_UID, ort);
            SqlCommand cmd = new SqlCommand(insertString, con);
            cmd.ExecuteNonQuery();
 
            // 3. Daten in lehrer_Exkursion speichern
            string insertString2 = string.Format(
               @"INSERT INTO [dbo].[lehrer_exkursion]
            (
                    [lehrer_UID],
                    [exkursion_UID]
                       )
            VALUES  (
                    '{0}',
                    '{1}'
                    )", LehrerUID, exkursion_UID);
            SqlCommand cmd2 = new SqlCommand(insertString2, con);
            cmd2.ExecuteNonQuery();

            //Daten in die Schueler- Exkursion tabelle eintragen

            DatenLaden d = new DatenLaden();
            List<Guid> SchuelerListe = d.LadeAlleSchuelerUID(kurs_UID);

            foreach (Guid schueler_UID in SchuelerListe)
            {
                string insertStringSchuelerExkursion = string.Format(
                   @"INSERT INTO [dbo].[schueler_exkursion]
            (
                    [schueler_UID],
                    [exkursion_UID],
                    [nimmtteil],
                    [hatGesehen]
                       )
            VALUES  (
                    '{0}',
                    '{1}',
                    'false',
                    'false'
                    )", schueler_UID, exkursion_UID);


                SqlCommand cmdSchuelerExkursion = new SqlCommand(insertStringSchuelerExkursion, con);
                cmdSchuelerExkursion.ExecuteNonQuery();
            }


            con.Close();

           /* string insertString3 = string.Format(
            @"INSERT INTO [dbo].[schueler_exkursion]
            (
                    [schueler_UID],
                    [kurs_UID]
                       )
            
            VALUES  (
                    '{0}',
                    '{1}'
                    )", schueler_UID, kurs_UID);
            SqlCommand cmd3 = new SqlCommand(insertString3, con);
            cmd.ExecuteNonQuery();
            con.Close();*/
            //Ich weiß nicht ob das richtig ist. Brauche eine Antwort von euch
        }


        internal void SpeichernNeuesPass(string schueler_emailaddresse, string passwort) {
            SqlConnection conn = createConnection();
            conn.Open();

           string updatestring = string.Format(
               @"UPDATE [dbo].[schueler]
                SET passwort = '{0}'
                WHERE schueler_emailadresse = '{1}'
            ",passwort, schueler_emailaddresse);
               
            SqlCommand command = new SqlCommand(updatestring, conn);
            command.ExecuteNonQuery();
            conn.Close();
        }


        internal void SpeichernDerAnmeldungLehrer(string name, string vorname, string email, string passwort) {
            SqlConnection conn = createConnection();
            conn.Open();

            string insertString = string.Format(
                       @"INSERT INTO [dbo].[lehrer]
            (
                    [name]
                    ,[vorname]
                    ,[email]
                    ,[passwort]    )
            
            VALUES  (
                    '{0}', 
                    '{1}',
                    '{2}',
                    '{3}'
                    ) ", name, vorname, email, passwort);

            SqlCommand command = new SqlCommand(insertString, conn);

            command.ExecuteNonQuery();
            conn.Close();
        }


        #region identischerCode_SchönererStil
        internal void SpeichernDerAnmeldungBessererStyle(string name, string nachname, string passwort, string email)
        {
            SqlConnection conn = createConnection();

            conn.Open();
            string insertString = createInsertCommand(name, nachname, passwort, email);

            SqlCommand command = new SqlCommand(insertString, conn);

            command.ExecuteNonQuery();

            conn.Close();
        }

        private static string createInsertCommand(string name, string nachname, string passwort, string email)
        {
            return string.Format(
                       @"INSERT INTO [dbo].[schueler]
            (
                    [schuelername]
                    ,[schuelernachname]
                    ,[passwort]
                    ,[schueler_emailadresse]    )
            
            VALUES  (
                    '{0}', 
                    '{1}',
                    '{2}',
                    '{3}'
                    ) ", name, nachname, passwort, email);
        }

        public static SqlConnection createConnection()
        {
            SqlConnectionStringBuilder builder = createBuilder();
            //Console.WriteLine(builder.ConnectionString);

            string meinTollerConnectionString = builder.ConnectionString;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = meinTollerConnectionString;
            return conn;
        }

       

        private static SqlConnectionStringBuilder createBuilder()
        {
            SqlConnectionStringBuilder builder =
            new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Data Source"] = /*@"Bärbel-PC\DIGILAUF"*/@"D3\SQLEXPRESS";
            builder["integrated Security"] = false;
            builder["Initial Catalog"] = "LaufzetData";
            builder["User"] = "sa";
            builder["Password"] = /*"paul"*/"florim";
            return builder;
        } 
        #endregion
    }
}