﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace DigitalerLaufzettel.Controller
{
    public class DatenLaden
    {
        private SqlConnection connection = null;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public DatenLaden()
        {
            connection = Speichern.createConnection();

        }


        public List<string[]> LadeAlleKurse()
        {

            connection.Open();
            string Sql = string.Format(@"Select kurs_UID, name
            FROM kurs
            ");

            SqlCommand command = new SqlCommand(Sql, connection);

            //generischer aufruf einer liste

            List<string[]> retList = new List<string[]>();
           
 
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                string[] tempItem = new string[2];
                tempItem[0] = reader.GetGuid(0).ToString();
                tempItem[1] = reader.GetString(1);

                retList.Add(tempItem);
             
            }
            
            connection.Close();

            return retList;
        }

        public List<string[]> termineLaden() {
            connection.Open();

            string QueryString = @"Select termin_UID, Tag, Stundenband
                                    From kurstermin
                                  ";

            SqlCommand command = new SqlCommand(QueryString, connection);
            SqlDataReader reader = command.ExecuteReader();


            List<string[]> alleTermine = new List<string[]>(); 

            while (reader.Read()) {
                string[] tempArray = new string[2];
                tempArray[0] = reader.GetString(1) + " " + reader.GetInt32(2);
                tempArray[1] = reader.GetGuid(0).ToString();
                alleTermine.Add(tempArray);

            }

            connection.Close();

            return alleTermine;

        }

        public List<string[]> ausstehendeExkursion(Guid schueler_UID) {
            connection.Open();

            string QueryString = string.Format(@" SELECT exkursion_UID
                        FROM            schueler_exkursion
                        WHERE           (hatGesehen LIKE 'false') AND (schueler_UID LIKE '{0}')", schueler_UID);

            SqlCommand command = new SqlCommand(QueryString, connection);

            SqlDataReader reader = command.ExecuteReader();

            List<Guid> tempList = new List<Guid>();

            while (reader.Read()) {
                Guid tempGuid = reader.GetGuid(0);
                tempList.Add(tempGuid);
            }

            List<string[]> tempListe = new List<string[]>();

            connection.Close();

            foreach (Guid exkursion_UID in tempList)
            {
                string[] tempstring = new string[2];
                tempstring[0] = exkursionsdaten(exkursion_UID);
                tempstring[1] = exkursion_UID.ToString();

                tempListe.Add(tempstring);
            }

          
            
            return tempListe;
        }

        public List<string[]> alleExkursionsanfragenLaden(Guid Lehrer_UID) {
            connection.Open();
            string Querystring = string.Format(@"SELECT        schueler_UID, exkursion_UID
                FROM            lehrer_erlaubt_exkursion
                WHERE        lehrer_UID LIKE '{0}' AND gelesen = 'False'", Lehrer_UID);

            SqlCommand command = new SqlCommand(Querystring, connection);
            SqlDataReader reader = command.ExecuteReader();

            List<Guid[]> infoliste = new List<Guid[]>();

            while (reader.Read()) {
                Guid[] tempstring = new Guid[2];
                tempstring[0] = reader.GetGuid(0);
                tempstring[1] = reader.GetGuid(1);
                infoliste.Add(tempstring);
            }
            reader.Close();
            connection.Close();

            List<String[]> stringInfos = new List<string[]>();
            foreach (Guid[] GuidArray in infoliste) {
                String[] tempstring = new string[2];
                tempstring[0] = schuelername(GuidArray[0]);
                tempstring[1] = exkursionsname(GuidArray[1]);
                stringInfos.Add(tempstring);

            }

             
            

            return stringInfos;
            
        }

        public string schuelername(Guid schueler_UID) {
            connection.Open();
            string Querystring = string.Format(@"SELECT schuelername, schuelernachname 
                    FROM            schueler
                    Where   schueler_UID = '{0}'", schueler_UID);
            SqlCommand command = new SqlCommand(Querystring, connection);
            SqlDataReader reader = command.ExecuteReader();

            string tempstring = string.Empty;
            if (reader.Read()) {
                tempstring = reader.GetString(0) + " " + reader.GetString(1);
                }

            
            connection.Close();

            return tempstring;

        }

        public string exkursionsname(Guid exkursion_UID)
        {
            connection.Open();
            string Querystring = string.Format(@"SELECT ort 
                    FROM            exkursion
                    Where   exkursion_UID = '{0}'", exkursion_UID);
            SqlCommand command = new SqlCommand(Querystring, connection);
            SqlDataReader reader = command.ExecuteReader();

            string tempstring = string.Empty;
            if (reader.Read())
            {
                tempstring = reader.GetString(0);
            }


            connection.Close();

            return tempstring;

        }



        public string exkursionsdaten(Guid exkursion_UID) {
            connection.Open();

            string QueryStringInformationenExkursion = string.Format(@"SELECT        datum, ort
                        FROM            exkursion
                        WHERE        (exkursion_UID LIKE '{0}')", exkursion_UID);

            SqlCommand tempCommand = new SqlCommand(QueryStringInformationenExkursion, connection);
            SqlDataReader readerInformationExkursion = tempCommand.ExecuteReader();
            string[] tempArray = new string[1];
            tempArray[0] = string.Empty;
            if (readerInformationExkursion.Read())
            {
                tempArray[0] = string.Format(readerInformationExkursion.GetDateTime(0).ToShortDateString() + " " + readerInformationExkursion.GetString(1));
            }
            
            connection.Close();
            return tempArray[0];
        }

        public List<Guid> LadeAlleSchuelerUID(Guid kurs_UID)
        {
            connection.Open();
            string QueryString = string.Format(@"Select schueler.schueler_UID
                        From schueler 
                        Inner Join schueler_kurs on schueler.schueler_UID = schueler_kurs.schueler_UID 
                        Inner Join kurs on schueler_kurs.kurs_UID = kurs.kurs_UID

                        Where kurs.kurs_UID Like '{0}'", kurs_UID);

            SqlCommand command = new SqlCommand(QueryString, connection);
            SqlDataReader reader = command.ExecuteReader();

            List<Guid> guidListe = new List<Guid>();

            while (reader.Read()) {
                Guid tempGuid = reader.GetGuid(0);
                guidListe.Add(tempGuid);
            }

            connection.Close();
            return guidListe;

        }

        public Guid LehrerUIDÜberEmail(string email) {
            connection.Open();

            string QueryString = string.Format(@"Select lehrer_UID
                        FROM lehrer
                        Where email LIKE '{0}'", email);

            SqlCommand command = new SqlCommand(QueryString, connection);
            
            SqlDataReader reader = command.ExecuteReader();
            
            Guid ausgeleseneUID = Guid.Empty;
            if (reader.Read()) {
                ausgeleseneUID = reader.GetGuid(0);
            }

            connection.Close();

            return ausgeleseneUID; 

        }

        public Guid SchülerUIDÜberEmail(string email)
        {
            connection.Open();

            string QueryString = string.Format(@"Select schueler_UID
                        FROM schueler
                        Where schueler_emailadresse LIKE '{0}'", email);

            SqlCommand command = new SqlCommand(QueryString, connection);


            SqlDataReader reader = command.ExecuteReader();

            

            Guid ausgeleseneUID = Guid.Empty;
            if (reader.Read())
            {
                ausgeleseneUID = reader.GetGuid(0);
            }

            connection.Close();

            return ausgeleseneUID;


        }
    }
}