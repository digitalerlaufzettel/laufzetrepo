﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DigitalerLaufzettel.Controller;
using System.Data.SqlClient;

namespace DigitalerLaufzettel.Controller
{
    public static class EMailPruefen
    {
        
        public static bool textpruefen(string text) {
            return System.Text.RegularExpressions.Regex.IsMatch(text, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
        
    }

}