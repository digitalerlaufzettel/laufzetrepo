﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DigitalerLaufzettel.Controller;
using System.Data.SqlClient;

namespace DigitalerLaufzettel.Controller
{
    public class IstEmailNochVorhanden
    {
        private SqlConnection connection = null;

        public IstEmailNochVorhanden()
        {
            connection = Speichern.createConnection();
        }

        public bool lehrerEmailTest(string email) {

            connection.Open();
            string anfrageLehrer = string.Format(@"SELECT email
            FROM lehrer
            Where email LIKE '{0}'", email);

            SqlCommand command2 = new SqlCommand(anfrageLehrer, connection);
            SqlDataReader reader2 = command2.ExecuteReader();
            string lehrerEmail = string.Empty;

            if (reader2.Read())
            {
                lehrerEmail = reader2.GetString(0);
            }

            connection.Close();

            if (lehrerEmail == email)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        public bool emailTest(string email)
        {
            connection.Open();
            
            string anfrage = string.Format(@"SELECT schueler_emailadresse
            FROM schueler
            Where schueler_emailadresse LIKE '{0}'", email);

            SqlCommand command = new SqlCommand(anfrage, connection);

            SqlDataReader reader = command.ExecuteReader();

            string ausgeleseneEmail = string.Empty;

            if (reader.Read())
            {
                ausgeleseneEmail = reader.GetString(0);
            }

            connection.Close();
            
            if (ausgeleseneEmail == email)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
