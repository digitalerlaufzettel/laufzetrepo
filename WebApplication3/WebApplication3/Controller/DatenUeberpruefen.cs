﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace DigitalerLaufzettel.Controller
{
    public class DatenUeberpruefen
    {
        private SqlConnection connection = null;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public DatenUeberpruefen() {
            connection = Speichern.createConnection();
        }

        public bool ueberpruefen(string email, string passwort) {

            connection.Open();
            string Sql = string.Format(@"Select passwort
            FROM schueler
            WHERE [schueler_emailadresse] = '{0}'", email);

            SqlCommand command = new SqlCommand(Sql, connection);

            string readPasswort = string.Empty;

            
            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read()) {
                readPasswort = reader.GetString(0);
                }

           
            connection.Close();

            if (passwort.Equals(readPasswort))
            {
                return true;
            }
            else {
                return false;
            }
        }



        public bool lehrerueberpruefen(string email, string passwort) {

            connection.Open();
            string Sql = string.Format(@"Select passwort
            FROM lehrer
            WHERE [email] = '{0}'", email);

            SqlCommand command = new SqlCommand(Sql, connection);

            string readPasswort = string.Empty;


            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                readPasswort = reader.GetString(0);
            }


            connection.Close();

            if (passwort.Equals(readPasswort))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        
        







    }
}