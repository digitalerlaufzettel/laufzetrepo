﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace DigitalerLaufzettel.Modelle
{
    public class Kurs_Loader
    {
        private SqlConnection connection = null;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public Kurs_Loader()
        {
            connection = Speichern.createConnection();

        }

        public List<Kurs> LadeAlleKurse()
        {

            connection.Open();
            string Sql = string.Format(@"Select kurs_UID, name
            FROM kurs
            ");

            SqlCommand command = new SqlCommand(Sql, connection);

            //generischer aufruf einer liste

            List<Kurs> retList = new List<Kurs>();


            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                //Kurs tempKurs = new Kurs();
                //tempKurs.Kurs_UID = reader.GetGuid(0);
                //tempKurs.Name = reader.GetString(1);
                //retList.Add(tempKurs);

                retList.Add(new Kurs(reader.GetGuid(0), reader.GetString(1)));
            }

            connection.Close();

            return retList;
        }
    }
}