﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DigitalerLaufzettel.Controller;

namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm9 : System.Web.UI.Page
    {
   
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void button_login_Click(object sender, EventArgs e)
        {
            if (textbox_emai.Text == "E-Mail Adresse" || textbox_emai.Text == "" || !EMailPruefen.textpruefen(textbox_emai.Text))
            {
                MessageBox.Show("Ungültige E-Mail");
                return;
            }

            if (textbox_Passwort.Text == "Password" || textbox_Passwort.Text == "")
            {
                MessageBox.Show("Ungültiges Passwort");
                return;
            }


            Session["Email"] = textbox_emai.Text;
            Session["Passwort"] = textbox_Passwort.Text;
            
            DigitalerLaufzettel.Controller.DatenUeberpruefen d = new Controller.DatenUeberpruefen();
            DigitalerLaufzettel.Controller.DatenLaden dl = new Controller.DatenLaden();
            string temp = Kryptographie.MD5(textbox_Passwort.Text);

            if (checkbox_Lehrer.Checked) {
                if (d.lehrerueberpruefen(textbox_emai.Text, temp)) {
                    Session["IstLehrer"] = "Ja";
                    Session["Lehrer_UID"] = dl.LehrerUIDÜberEmail(textbox_emai.Text);
                    Response.Redirect(Targets.neuesFensterFürLehrer);
                }
                else
                {
                    MessageBox.Show("Ungültiges Passwort");
                    return;
                }
            }
            else
            {
                if (d.ueberpruefen(textbox_emai.Text, temp))
                {
                    Session["IstLehrer"] = "Nein";
                    Session["Schueler_UID"] = dl.SchülerUIDÜberEmail(textbox_emai.Text);
                    Response.Redirect(Targets.xyNeuesfensterForm);
                    }
                else
                {
                    MessageBox.Show("Ungültiges Passwort");
                    return;
                }
            }




           
        }

        protected void button_SessionTest_Click(object sender, EventArgs e)
        {
            if (Session["Schueler_UID"] != null) {
                label_SessionTest.Text = Session["Schueler_UID"].ToString();

            } else {
                label_SessionTest.Text = "Es ist zur Zeit keine SChueler_UID in der Session gespeichert.";

            }
        }
    }
}