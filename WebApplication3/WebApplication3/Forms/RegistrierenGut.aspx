﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrierenGut.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm6" %>

<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<title>Best Login Page design in html and css</title>
<style type="text/css">
body {
background-color: #f4f4f4;
color: #5a5656;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 16px;
line-height: 1.5em;
}
a { text-decoration: none; }
h1 { font-size: 1em; }
h1, p {
margin-bottom: 10px;
}
strong {
font-weight: bold;
}
.uppercase { text-transform: uppercase; }

/* ---------- LOGIN ---------- */
#login {
margin: 50px auto;
width: 300px;
}
form fieldset input[type="text"], input[type="password"] {
background-color: #e5e5e5;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #5a5656;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 14px;
height: 50px;
outline: none;
padding: 0px 10px;
width: 280px;
-webkit-appearance:none;
}
form fieldset input[type="submit"] {
background-color: #008dde;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #f4f4f4;
cursor: pointer;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
height: 50px;
text-transform: uppercase;
width: 300px;
-webkit-appearance:none;
}
form fieldset a {
color: #5a5656;
font-size: 10px;
}
form fieldset a:hover { text-decoration: underline; }
.btn-round {
background-color: #5a5656;
border-radius: 50%;
-moz-border-radius: 50%;
-webkit-border-radius: 50%;
color: #f4f4f4;
display: block;
font-size: 12px;
height: 50px;
line-height: 50px;
margin: 30px 125px;
text-align: center;
text-transform: uppercase;
width: 50px;
}
.facebook-before {
background-color: #0064ab;
border-radius: 3px 0px 0px 3px;
-moz-border-radius: 3px 0px 0px 3px;
-webkit-border-radius: 3px 0px 0px 3px;
color: #f4f4f4;
display: block;
float: left;
height: 50px;
line-height: 50px;
text-align: center;
width: 50px;
}
.facebook {
background-color: #0079ce;
border: none;
border-radius: 0px 3px 3px 0px;
-moz-border-radius: 0px 3px 3px 0px;
-webkit-border-radius: 0px 3px 3px 0px;
color: #f4f4f4;
cursor: pointer;
height: 50px;
text-transform: uppercase;
width: 250px;
}
.twitter-before {
background-color: #189bcb;
border-radius: 3px 0px 0px 3px;
-moz-border-radius: 3px 0px 0px 3px;
-webkit-border-radius: 3px 0px 0px 3px;
color: #f4f4f4;
display: block;
float: left;
height: 50px;
line-height: 50px;
text-align: center;
width: 50px;
}
.twitter {
background-color: #1bb2e9;
border: none;
border-radius: 0px 3px 3px 0px;
-moz-border-radius: 0px 3px 3px 0px;
-webkit-border-radius: 0px 3px 3px 0px;
color: #f4f4f4;
cursor: pointer;
height: 50px;
text-transform: uppercase;
width: 250px;
}
</style>
</head>
<body>
<div id="login">
<h1><strong>Welcome.</strong> Please register.</h1>
    <form id="form1" runat="server">
<fieldset> 
<p><asp:TextBox runat ="server" value="Name" onBlur="if(this.value=='')this.value='Name'" onFocus="if(this.value=='Name')this.value=''" ID="textbox_Name" OnClick="textbox_Name_Click" AutoPostBack="True"></asp:TextBox></p>
<p><asp:TextBox runat ="server" type="text" value="Vorname" onBlur="if(this.value=='')this.value='Vorname'" onFocus="if(this.value=='Vorname')this.value='' " ID="textbox_Vorname" ></asp:TextBox></p>
<p><asp:TextBox runat ="server" type="text" value="E-Mail" onBlur="if(this.value=='')this.value='E-Mail'" onFocus="if(this.value=='E-Mail')this.value='' " ID="textbox_Email"></asp:TextBox></p>
<p><asp:TextBox runat ="server" type="password" value="Passwort" onBlur="if(this.value=='')this.value='Passwort'" onFocus="if(this.value=='Passwort')this.value='' " ID="textbox_Passwort"></asp:TextBox></p>
<p><asp:TextBox runat ="server" type="password" value="Password"  ID="textbox_Passwortwiederholen" onBlur="if(this.value=='')this.value='Password'" onFocus="if(this.value=='Password')this.value='' "></asp:TextBox> 
    <asp:CheckBox ID="checkbox_Lehrer" runat="server" Text="Lehrer" />
    </p>
<p>
<asp:Button type="submit" ID="button_Speichern" runat="server" OnClick="Button1_Click1" Text="Registrieren" />
  
</fieldset>
    </form>


</div> <!-- end login -->
</body>
    <script>


    </script>

</html>
