﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DigitalerLaufzettel.Controller;

namespace DigitalerLaufzettel.Forms
{
    public partial class exkursionAnlegen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        

        protected void button_speichern_Click1(object sender, EventArgs e)
        {
            Speichern s = new Speichern();
            //(Guid kurs_UID, string tag, int beginn_stundenband, int ende_stundenband, DateTime datum, Guid LehrerUID, string ort) {
            Guid kurs_UID = new Guid(dropbox_kurs.SelectedItem.Value);
            string tag = datumsauswahl.SelectedDate.DayOfWeek.ToString();
            int beginn_stundenband = int.Parse(textbox_stundenband_anfang.Text);
            int ende_stundenband = int.Parse(textbox_stundenband_ende.Text);

            DateTime datum = datumsauswahl.SelectedDate;
            Guid LehrerUID = new Guid(Session["Lehrer_UID"].ToString());
            string ort = textbox_Ort.Text;
            s.SpeichernExkursion( kurs_UID, tag,beginn_stundenband,  ende_stundenband,  datum,  LehrerUID,  ort);
            
        }

        protected void datumsauswahl_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void dropbox_kurs_Init1(object sender, EventArgs e)
        {
            dropbox_kurs.Items.Clear();
            // Im zweitn Parameter kann man was erstecken, nur der erste wird angezeigt 

            List<string[]> tempList = new DatenLaden().LadeAlleKurse();

            foreach (string[] tempItem in tempList)
            {
                dropbox_kurs.Items.Add(new ListItem(tempItem[1], tempItem[0]));

            }
        }
    }
}