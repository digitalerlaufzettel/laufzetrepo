﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KennwortAendern.aspx.cs" Inherits="DigitalerLaufzettel.Forms.KennwortAendern" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="altpasswort" runat="server" Text="Altes Passwort"></asp:Label>
            <asp:TextBox ID="altpass" runat="server"></asp:TextBox>
        </div>
        <p>
            <asp:Label ID="neuespasswort" runat="server" Text="Neus Passwort"></asp:Label>
            <asp:TextBox ID="neupass" runat="server"></asp:TextBox>
        </p>
        <asp:Label ID="neuespasswortwied" runat="server" Text="Neues Passwort wiederholen"></asp:Label>
        <asp:TextBox ID="neupasswied" runat="server"></asp:TextBox>
        <p>
            <asp:Button ID="passänd_button" runat="server" Text="Passwort ändern" OnClick="passänd_button_Click" />
        </p>
    </form>
</body>
</html>
