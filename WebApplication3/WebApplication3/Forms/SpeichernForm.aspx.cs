﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DigitalerLaufzettel.Controller;
namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }
        


        protected void button_Speichern_Click(object sender, EventArgs e)
        {
            if (textbox_Name.Text == "") {
                label_fehlerfeld.Text = "Der Name Fehlt";
                return;
                    }
            if (textbox_Vorname.Text == "") {
                label_fehlerfeld.Text = "Der Vorname fehlt";
                return;


            }
            if (!(textbox_Passwort.Text == textbox_passwortwiederholen.Text))
            {
                label_fehlerfeld.Text = "ungültiges zweites Passwort";
                return;
            }
            if (!EMailPruefen.textpruefen(textbox_emailaddresse.Text))
            {
                label_fehlerfeld.Text = "ungültige Email";
                return;
            }
            else
            {

                string verschluesseltesPasswort = Kryptographie.MD5(textbox_Passwort.Text);
                new Speichern().SpeichernDerAnmeldung(textbox_Vorname.Text, textbox_Name.Text, verschluesseltesPasswort, textbox_emailaddresse.Text);
                Response.Redirect(Targets.xyNeuesfensterForm);
            }
        }
    }
}