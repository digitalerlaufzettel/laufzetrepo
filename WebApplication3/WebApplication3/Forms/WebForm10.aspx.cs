﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DigitalerLaufzettel.Controller;
using System.Web.UI.WebControls;

namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
   
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ListItem tempItem = (ListItem)

            ListItem tempItem = (DropDownList1.SelectedItem);
            if (tempItem.Value != null)
            {
                Label1.Text = tempItem.Value;
            }
        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            ListItem tempItem = (DropDownList1.SelectedItem);
            if (tempItem.Value != null)
            {
                Label1.Text = tempItem.Value;
            }
        }

        protected void DropDownList1_Init(object sender, EventArgs e)
        {
            DropDownList1.Items.Clear();
            // Im zweitn Parameter kann man was erstecken, nur der erste wird angezeigt 

            List<string[]> tempList = new DatenLaden().LadeAlleKurse();

            foreach (string[] tempItem in tempList) {
                DropDownList1.Items.Add(new ListItem(tempItem[1], tempItem[0]));

            }

        }

        protected void DropDownList1_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ListItem tempItem = (DropDownList1.SelectedItem);
            if (tempItem.Value != null)
            {
                Label1.Text = tempItem.Value;
            }
        }
    }
}