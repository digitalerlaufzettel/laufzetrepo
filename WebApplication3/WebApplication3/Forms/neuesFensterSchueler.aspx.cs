﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DigitalerLaufzettel.Controller;
using System.Web.UI.WebControls;

namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session != null)
            //{
            //    label_Session.Text = Session["Email"].ToString();
            //}
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["Schueler_UID"] != null)
            {
                label_Session.Text = Session["Schueler_UID"].ToString();
            }
        }

        protected void DropDownList1_Init(object sender, EventArgs e)
        {
            DropDownList1.Items.Clear();
            // Im zweitn Parameter kann man was erstecken, nur der erste wird angezeigt 

            List<string[]> tempList = new DatenLaden().LadeAlleKurse();

            foreach (string[] tempItem in tempList)
            {
                DropDownList1.Items.Add(new ListItem(tempItem[1], tempItem[0]));


            }
        }

        protected void button_Kursbeitreten_Click(object sender, EventArgs e)
        {
            Speichern s = new Speichern();
            s.SpeichernKursFuerSchueler(new Guid(Session["Schueler_UID"].ToString()), new Guid(DropDownList1.SelectedItem.Value));
        }

        protected void dropdownlist_Exkursionen_Init(object sender, EventArgs e)
        {
            List<string[]> tempList = new DatenLaden().ausstehendeExkursion(new Guid(Session["Schueler_UID"].ToString()));

            foreach (string[] tempItem in tempList)
            {
                dropdownlist_Exkursionen.Items.Add(new ListItem(tempItem[0], tempItem[1]));
            }
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Startseite.aspx");
        }

        protected void button_ExkursionBeitreten_Click(object sender, EventArgs e)
        {
            Speichern s = new Speichern();
            s.SchuelerBestätigtExkursion("Montag", 1,5, new Guid(Session["Schueler_UID"].ToString()), new Guid(dropdownlist_Exkursionen.SelectedItem.Value));



        }

        protected void button_PasswortÄndern_Click(object sender, EventArgs e)
        {
            Response.Redirect("KennwortAendern.aspx");
        }
    }
}