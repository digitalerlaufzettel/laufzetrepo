﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpeichernForm.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 101px;
        }
        .auto-style2 {
            width: 138px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 62%; height: 101px;">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label1" runat="server" Text="Vorname"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="textbox_Vorname" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="label_fehlerfeld" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="Name               "></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="textbox_Name" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <p style="width: 72px">
                        Passwort</p>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="textbox_Passwort" TextMode="Password" runat="server" Rows="1"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server"  Text="Passwort Wiederholen"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="textbox_passwortwiederholen" TextMode="Password" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
             <td>E-Mail </td>
             <td>
                 <asp:TextBox ID="textbox_emailaddresse" runat="server"></asp:TextBox>
            </td>
             
        </table>
        <p>
            <asp:Button ID="button_Speichern" runat="server" OnClick="button_Speichern_Click" Text="Speichern" Width="239px" />
        </p>
      
    </form>
</body>
</html>
