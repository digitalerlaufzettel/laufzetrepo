﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Einloggen.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm4" Theme="" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 80px;
        }
        .auto-style2 {
            width: 279px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%; height: 139px;">
                <tr>
                    <td class="auto-style1">E-Mail</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="textbox_email" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">Passwort</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="textbox_passwort" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="label_info" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style2">
                        <asp:Button ID="butten_Anmelden" runat="server" OnClick="butten_Anmelden_Click" Text="Anmelden" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
