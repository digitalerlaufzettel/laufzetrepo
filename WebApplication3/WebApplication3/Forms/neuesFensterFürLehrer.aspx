﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="neuesFensterFürLehrer.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm7" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Das ist das neue Fenster für Lehrer.</div>
        <asp:Button ID="button_kursAnlegen" runat="server" Text="KursAnlegen" OnClick="button_kursAnlegen_Click" />
        <asp:Button ID="Button2" runat="server" Text="Exkursion Ausstellen" OnClick="Button2_Click" />
        <p>
            <asp:Button ID="button_abmelden" runat="server" OnClick="button_abmelden_Click" Text="Abmelden" />
        </p>
        <asp:DropDownList ID="dropdownlist_ExkursionGenehmigen" runat="server" OnInit="dropdownlist_ExkursionGenehmigen_Init">
        </asp:DropDownList>
    </form>
</body>
</html>
