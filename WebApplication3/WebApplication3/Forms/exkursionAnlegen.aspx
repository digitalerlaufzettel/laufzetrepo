﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="exkursionAnlegen.aspx.cs" Inherits="DigitalerLaufzettel.Forms.exkursionAnlegen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <asp:TextBox ID="Datum" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
        <asp:Label ID="Label1" runat="server" Text="Datum"></asp:Label>
        <p>
            <asp:TextBox ID="textbox_stundenband_anfang" runat="server"></asp:TextBox>
            <asp:Label ID="Label2" runat="server" Text="Stundenbandbeginn"></asp:Label>
        </p>
        <asp:TextBox ID="textbox_stundenband_ende" runat="server"></asp:TextBox>
        <asp:Label ID="Label3" runat="server" Text="Stundenbandende"></asp:Label>
        <p>
            <asp:TextBox ID="textbox_Ort" runat="server"></asp:TextBox>
            <asp:Label ID="Label4" runat="server" Text="Ort"></asp:Label>
        </p>
        <asp:DropDownList ID="dropbox_kurs" runat="server" AutoPostBack = "true" OnInit="dropbox_kurs_Init1">
        </asp:DropDownList>
        <asp:Button ID="button_speichern" runat="server" Text="Speichern" OnClick="button_speichern_Click1" />
        <asp:Calendar ID="datumsauswahl" runat="server" OnSelectionChanged="datumsauswahl_SelectionChanged"></asp:Calendar>
    </form>
</body>
</html>
