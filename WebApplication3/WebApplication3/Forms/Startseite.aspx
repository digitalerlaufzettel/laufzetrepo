﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Startseite.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm9" %>

<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<title>Digitaler Laufzettel</title>
<style type="text/css">
body {
color: #5a5656;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 16px;
line-height: 1.5em;
}
a { text-decoration: none; }
h1 { font-size: 1em; }
h1, p {
margin-bottom: 10px;
}
strong {
font-weight: bold;
}
.uppercase { text-transform: uppercase; }

/* ---------- LOGIN ---------- */
#login {
margin: 50px auto;
width: 300px;
}
form fieldset input[type=text], input[type=password] {
background-color: #e5e5e5;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #5a5656;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 14px;
height: 50px;
outline: none;
padding: 0px 10px;
width: 280px;
-webkit-appearance:none;
}
form fieldset input[type=submit] {
background-color: #008dde;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #f4f4f4;
cursor: pointer;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
height: 50px;
text-transform: uppercase;
width: 300px;
-webkit-appearance:none;
}
form fieldset a {
color: #5a5656;
font-size: 10px;
}
form fieldset a:hover { text-decoration: underline; }
.btn-round {
background-color: #5a5656;
border-radius: 50%;
-moz-border-radius: 50%;
-webkit-border-radius: 50%;
color: #f4f4f4;
display: block;
font-size: 12px;
height: 50px;
line-height: 50px;
margin: 30px 125px;
text-align: center;
text-transform: uppercase;
width: 50px;
}
.auto-style1 {
	text-align: right;
}
</style>


</head>
<body style="background-color: #FFFFFF">
    <!-- end login -->
<p class="auto-style1"></p>
    
  
      
    
<div id="login">
    <form id="form1" runat="server">
          <asp:Button ID="button_SessionTest" runat="server" OnClick="button_SessionTest_Click" Text="Button" />
    <asp:Label ID="label_SessionTest" runat="server" Text="Label"></asp:Label>
<h1><strong>Welcome.</strong> Please login.</h1>
		<div class="auto-style1">
<fieldset>
<p><asp:TextBox runat="server" type="text" required value="E-Mail Adresse" onBlur="if(this.value=='')this.value='E-Mail Adresse'" onFocus="if(this.value=='E-Mail Adresse')this.value='' " ID="textbox_emai"></asp:TextBox></p>

<p>
<asp:TextBox ID="textbox_Passwort"  runat="server" onBlur="if(this.value=='')this.value='Password'"  onFocus="if(this.value=='Password')this.value='' " required="" type="password" value="Password"></asp:TextBox>
    <asp:CheckBox ID="checkbox_Lehrer" runat="server" Text="Lehrer" />
    </p>

<!--<p><a href="#">Passwort vergessen?/</a>-->
             <asp:HyperLink id="hyperlink2" 
                  NavigateUrl="http://localhost:50766/Forms/KennwortAendern.aspx"
                  Text="Passwort ändern"
                  runat="server"/> 


              <asp:HyperLink id="hyperlink1" 
                  NavigateUrl="http://localhost:50766/Forms/RegistrierenGut.aspx"
                  Text="Registrieren"
                  runat="server"/> 

<asp:Button type="submit" ID="button_login" runat="server" BackColor="Aqua" OnClick="button_login_Click" Text="Login" />
  
</fieldset>
    </div>
    </form>

</div> 
</body>
</html>
