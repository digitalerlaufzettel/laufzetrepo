﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KursAnlegen.aspx.cs" Inherits="DigitalerLaufzettel.Forms.WebForm8" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="height: 166px; width: 456px">
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:TextBox ID="textbox_Kursname" runat="server"></asp:TextBox>
        <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
        <asp:Button ID="button_Speichern" runat="server" OnClick="button_Speichern_Click" Text="Neuen Kurs Speichern" />
        <br />
        <br />
        <asp:DropDownList ID="dropdownlist_ErsterTermin" runat="server" OnInit="dropdownlist_ErsterTermin_Init" >
        </asp:DropDownList>
        <asp:Label ID="Label2" runat="server" Text="Erster Termin"></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="dropdownlist_ZweiterTermin" runat="server">
        </asp:DropDownList>
        <asp:Label ID="Label3" runat="server" Text="Zweiter Termin"></asp:Label>
        <br />
        <br />
        <br />
        <br />
    </form>
</body>
</html>
