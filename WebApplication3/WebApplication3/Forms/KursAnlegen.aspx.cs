﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DigitalerLaufzettel.Controller;
using System.Web.UI.WebControls;

namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_Speichern_Click(object sender, EventArgs e)
        {
            Speichern s = new Speichern();
            s.SpeichernKurs(textbox_Kursname.Text, new Guid(Session["Lehrer_UID"].ToString()), new Guid(dropdownlist_ErsterTermin.SelectedItem.Value), new Guid(dropdownlist_ZweiterTermin.SelectedItem.Value));



        }

        protected void dropdownlist_ErsterTermin_Init(object sender, EventArgs e)
        {
            DatenLaden dl = new DatenLaden();
            List<string[]> terminListe = dl.termineLaden();

            foreach (string[] tempArray in terminListe) {
                dropdownlist_ErsterTermin.Items.Add(new ListItem(tempArray[0], tempArray[1]));
                dropdownlist_ZweiterTermin.Items.Add(new ListItem(tempArray[0], tempArray[1]));
            }



        }

    }
}