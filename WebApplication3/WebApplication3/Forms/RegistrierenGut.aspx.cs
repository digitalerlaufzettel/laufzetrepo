﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Windows.Media;


using DigitalerLaufzettel.Controller;
namespace DigitalerLaufzettel.Forms
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void textbox_Passwort_TextChanged(object sender, EventArgs e)
        {
            return;
        }


        protected void textbox_Name_TextChanged(object sender, System.EventArgs e)
        {
            //System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#5a5656");
            //Color c  = System.Drawing.ColorTranslator.FromHtml("#5a5656");
            //textbox_Name.BackColor = System.Drawing.Color.col;

            //textbox_Name.BackColor = System.Drawing.Color.Gray;


            

        }

        protected void textbox_Name_Click(object sender, System.EventArgs e) {
            textbox_Name.BackColor = System.Drawing.Color.Green;

        }





        protected void Button1_Click1(object sender, EventArgs e)
        {
            //MessageBox.Show(sender.ToString());
            //MessageBox.Show(e.ToString());
            //textbox_Name.BackColor = System.Drawing.Color.White;
            //textbox_Name.ForeColor = System.Drawing.Color.Red;
            //textbox_Name.BorderStyle = System.Web.UI.WebControls.BorderStyle.Double;

            

            if (textbox_Name.Text == "Name" || textbox_Name.Text == "")
            {
                MessageBox.Show("Ungültiger Name");
                //textbox_Name.BackColor = System.Drawing.Color.Red;
                return;
            }

            if (textbox_Vorname.Text == "" || textbox_Vorname.Text == "Vorname")
            {
                MessageBox.Show("Ungültiger Vorname");
                //textbox_Vorname.BackColor = System.Drawing.Color.Red;
                return;
            }

            if (!EMailPruefen.textpruefen(textbox_Email.Text))
            {
                MessageBox.Show("Ungültige Email");
                //textbox_Email.BackColor = System.Drawing.Color.Red;
                return;
            }

            if (textbox_Passwort.Text != textbox_Passwortwiederholen.Text)
            {
                MessageBox.Show("Ungültiges zweites Passwort");
                //textbox_Passwortwiederholen.BackColor = System.Drawing.Color.Red;
                return;
            }

            DigitalerLaufzettel.Controller.IstEmailNochVorhanden d = new Controller.IstEmailNochVorhanden();
            DigitalerLaufzettel.Controller.IstEmailNochVorhanden x = new Controller.IstEmailNochVorhanden();

            if (!d.emailTest(textbox_Email.Text) || !x.lehrerEmailTest(textbox_Email.Text))
            {
                MessageBox.Show("Diese Email ist leider schon vergeben");
                return;
            }

            if (checkbox_Lehrer.Checked)
            {
                string verschluesseltesPasswort = Kryptographie.MD5(textbox_Passwort.Text);
                new Speichern().SpeichernDerAnmeldungLehrer(textbox_Name.Text, textbox_Vorname.Text, textbox_Email.Text, verschluesseltesPasswort);
                Response.Redirect(Targets.Test123);
            }
            else
            {
                string verschluesseltesPasswort = Kryptographie.MD5(textbox_Passwort.Text);
                new Speichern().SpeichernDerAnmeldung(textbox_Vorname.Text, textbox_Name.Text, verschluesseltesPasswort, textbox_Email.Text);
                Response.Redirect(Targets.Test123);
            }

            //if (textbox_Vorname.Text == "")
            //{
            //    label_fehlerfeld.Text = "Der Vorname fehlt";
            //    return;


            //}
            //if (!(textbox_Passwort.Text == textbox_passwortwiederholen.Text))
            //{
            //    label_fehlerfeld.Text = "ungültiges zweites Passwort";
            //    return;
            //}
            //if (!EMailPruefen.textpruefen(textbox_emailaddresse.Text))
            //{
            //    label_fehlerfeld.Text = "ungültige Email";
            //    return;
            //}
            //else
            //{


            //}

        }

        
       
    }
}