﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigitalerLaufzettel
{
    public static class Targets
    {
        private static string serverUndPort = "http://localhost:50766/";
        private static string loginForm = "Forms/SpeichernForm.aspx";
        private static string neuesfensterForm = "Forms/neuesFensterSchueler.aspx";
        private static string login = "Forms/Einloggen.aspx";
        private static string test123 = "Forms/Startseite.aspx";
        private static string neuesFensterLehrer = "Forms/neuesFensterFürLehrer.aspx";
        private static string kursAnlegen = "Forms/KursAnlegen.aspx";
        private static string exkursionAnlegen = "Forms/exkursionAnlegen.aspx";



        public static string xyNeuesfensterForm {
            get { return string.Format("{0}{1}", serverUndPort, neuesfensterForm); }
            }

        public static string LoginForm {
            get { return string.Format("{0}{1}", serverUndPort, loginForm); }
            }
        public static string Login {
            get { return string.Format("{0}{1}", serverUndPort, login); }
        }

        public static string Test123 {
            get { return string.Format("{0}{1}", serverUndPort, test123); }
        }
        public static string neuesFensterFürLehrer
        {
            get { return string.Format("{0}{1}", serverUndPort, neuesFensterLehrer); }
        }
        public static string KursAnlegen
        {
            get { return string.Format("{0}{1}", serverUndPort, kursAnlegen); }
        }
        public static string ExkursionAusstellen
        {
            get { return string.Format("{0}{1}", serverUndPort, exkursionAnlegen); }
        }

    }
}